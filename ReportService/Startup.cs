﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReportService.Startup))]
namespace ReportService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
