﻿using ReportService.DAL;
using System.Collections.Generic;
using System.IO;

namespace ReportService.Models
{
    public class ReportVM
    {
        public Dictionary<ReportKind, ReportTaskVM> Reports { get; set; }
     
    }
}