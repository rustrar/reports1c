﻿using ReportService.DAL;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace ReportService.Models
{
    public class ReportTaskVM
    {        
        public ReportKind ReportKind { get; set; }
        public ReportTask ReportTask { get; set; }
        [DisplayName("Статус")]
        public string StatusMessage { get; set; }
        public string ClassValue { get; set; }
        public string ExistingMessageWarning { get; set; }

        public List<Category> Category { get; set; }        

        public IEnumerable<SelectListItem> Categories
        {
            get { return new SelectList(Category, "Id", "Name"); }
        }

        public List<Nomenclature> Nomenclatures { get; set; }
        public List<City> City { get; set; }
        public string Citi { get; set; }
        public string Param { get; set; }
        public IEnumerable<SelectListItem> CitiesVNR
        {
            get { return new SelectList(City.Where(c => (c.CategoryId == 1 || c.CategoryId == 2)), "Id", "Name"); }
        }
        public IEnumerable<SelectListItem> CitiesPiter
        {
            get { return new SelectList(City.Where(c => c.CategoryId == 3), "Id", "Name"); }
        }

        public List<UserCity> UserCity { get; set; }

        public IEnumerable<SelectListItem> CitiesCheckAnalytics
        {
            get
            {
                if (UserCity.Count == 0)                
                    return new SelectList(City, "Id", "Name");
                
                var listCitiesId = UserCity.Select(c => c.CityId).ToList();                
                return new SelectList(City.Where(c => listCitiesId.Contains(c.Id)), "Id", "Name");
            }
        }
        public List<ReportKind> ReportsSubChanges { get; set; }        
    }
}