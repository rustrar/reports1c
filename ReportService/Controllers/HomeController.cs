﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ReportService.DAL;
using ReportService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportService.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private EFReportServiceRepository repository = new EFReportServiceRepository();

        public ActionResult Index()
        {            
            return View(GetReporttasks());
        }

        [HttpGet]
        public ActionResult RefreshAndDownload()
        {
            return PartialView("ReportTaskPartialView", GetReporttasks());
        }

        private List<ReportTaskVM> GetReporttasks()
        {
            IList<string> roles = new List<string> { "Роль не определена" };
            List<ReportKind> reportKinds = new List<ReportKind>();
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            if (user != null)
                roles = userManager.GetRoles(user.Id);

            if (roles.Contains("Admin"))
                reportKinds = repository.ReportKinds.ToList();
            else if (roles.Contains("CheckAnalytics"))            
                reportKinds = repository.ReportKinds.Where(rk => !rk.IsAdmin).ToList();                            
            else
                reportKinds = repository.ReportKinds.Where(rk => !rk.IsAdmin && (rk.Id != 1023 || rk.Id != 1025)).ToList();

            var reportTasks = repository.ReportTasks.ToList();            
            var changeTask = repository.ReportTasks.Where(rt => rt.ReportKindId == 13 && rt.Status == "New").FirstOrDefault();

            if (changeTask != null)
                ViewBag.Change = "disabled";
            else
                ViewBag.Change = "";

            List<ReportTaskVM> listOfReportTaskVM = new List<ReportTaskVM>();

            foreach (var reportKind in reportKinds)
            {
                AddToCollection(listOfReportTaskVM, reportTasks, reportKind);
            }

            var today = DateTime.Today;
            ViewBag.LastSaturday = today.AddDays(-(int)today.DayOfWeek).AddDays(-1).ToString("yyyy-MM-dd");
            ViewBag.Friday = today.AddDays(-(int)today.DayOfWeek).AddDays(5).ToString("yyyy-MM-dd");
            ViewBag.Yesterday = today.AddDays(-1).ToString("yyyy-MM-dd");
            ViewBag.TopProductDate = DateTime.Now.DayOfWeek == System.DayOfWeek.Saturday ? today.AddDays(-1).ToString("yyyy-MM-dd") : today.ToString("yyyy-MM-dd");
            return listOfReportTaskVM.OrderBy(r => r.ReportKind.Order).ToList();
        }

        private void AddToCollection(List<ReportTaskVM> listOfReportTaskVM, IEnumerable<ReportTask> reportTasks, ReportKind reportKind)
        {
            var userId = User.Identity.GetUserId();
            var reportTaskVM = new ReportTaskVM();            
            reportTaskVM.Category = repository.Categories.ToList();
            reportTaskVM.City = repository.Cities.ToList();
            reportTaskVM.UserCity = repository.UserCities.Where(uc => uc.UserId == userId).ToList();
            //reportTaskVM.City.ToList().ForEach(c => c.IsChecked = true);
            reportTaskVM.ReportsSubChanges = repository.ReportKinds.Where(rk => rk.ParamType == "subchange" && rk.Id != 1032).ToList();
            reportTaskVM.Nomenclatures = repository.Nomenclatures.OrderBy(n => n.Name).ToList();
            reportTaskVM.ReportKind = reportKind;
            reportTaskVM.ReportTask = reportTasks.Where(rt => (rt.ReportKindId == reportKind.Id && rt.UserId == User.Identity.GetUserId())).FirstOrDefault();
            SetClass(reportTaskVM);
            if (reportTaskVM.ReportTask == null)
            {
                reportTaskVM.StatusMessage = "";                             
            }
            else
            {
                SetStatus(reportTaskVM);                
            }

            listOfReportTaskVM.Add(reportTaskVM);
        }

        private void SetClass(ReportTaskVM reportTaskVM)
        {            
            switch (reportTaskVM.ReportKind.ParamType)
            {
                case "subchange":
                    reportTaskVM.ClassValue = "none";
                    break;
                default:
                    reportTaskVM.ClassValue = "";
                    break;
            }

            switch (reportTaskVM.ReportKind.Size)
            {
                case "big":
                    reportTaskVM.ClassValue = reportTaskVM.ClassValue + " big";
                    break;
                case "small":
                    reportTaskVM.ClassValue = reportTaskVM.ClassValue + " small";
                    break;
                default:                    
                    break;
            }
        }

        private static void SetStatus(ReportTaskVM reportTaskVM)
        {
            switch (reportTaskVM.ReportTask.Status)
            {
                case "New":
                    reportTaskVM.StatusMessage = "Отчет формируется";
                    break;
                case "InProgress":
                    reportTaskVM.StatusMessage = "Отчет создается";
                    break;
                case "Error":
                    reportTaskVM.StatusMessage = "Ошибка создания отчета";
                    break;
                case "Done":
                    reportTaskVM.StatusMessage = "Отчет готов";
                    break;
                default:
                    reportTaskVM.StatusMessage = "";
                    break;
            }
        }

        [HttpPost]
        public ActionResult Create(ReportTaskVM reportTaskVM)
        {
            var userId = User.Identity.GetUserId();

            ReportTask existingReportTask = repository.ReportTasks.FirstOrDefault(r =>
                   (r.ReportKindId == reportTaskVM.ReportKind.Id
                    && r.UserId == userId
                    && (r.Status == "New") || (r.Status == "InProgress")));            
            
            if (existingReportTask != null)
            {                                
                return RedirectToAction("Index");                
            }

            DeleteReadyReport(reportTaskVM, userId);

            ReportTask reportTask = new ReportTask();
            reportTask.ReportKindId = reportTaskVM.ReportKind.Id;
            reportTask.UserId = userId;
            reportTask.Status = "New";
            reportTask.Created = DateTime.Now;

            if (reportTaskVM.ReportTask.ParamValue != null)
                if (reportTaskVM.ReportKind.Id == 1030)
                    reportTask.ParamValue = SetParam(reportTaskVM.ReportTask.ParamValue, reportTaskVM.Param);
                else
                    reportTask.ParamValue = SetParam(reportTaskVM);
                
                            
            repository.SaveReport(reportTask);

            return RedirectToAction("Index");
        }

        private string SetParam(ReportTaskVM reportTaskVM)
        {
            if (!string.IsNullOrEmpty(reportTaskVM.Citi))
                return string.Join(";", reportTaskVM.Citi, reportTaskVM.ReportTask.ParamValue);

            return reportTaskVM.ReportTask.ParamValue;
        }

        [HttpPost]
        public ActionResult CreateChange(ReportTaskVM reportTaskVM)
        {
            var userId = User.Identity.GetUserId();

            ReportTask existingReportTask = repository.ReportTasks.FirstOrDefault(r =>
                   (r.ReportKindId == reportTaskVM.ReportKind.Id
                    && r.UserId == userId
                    && (r.Status == "New") || (r.Status == "InProgress")));

            if (existingReportTask != null)
            {
                return RedirectToAction("Index");
            }

            DeleteReadyReport(reportTaskVM, userId);

            var selectedReports = reportTaskVM.ReportsSubChanges.Where(rsc => rsc.IsChecked).Select(rsc => rsc.Id).ToList();
            var selectedCity = reportTaskVM.City.Where(c => c.IsChecked).Select(c => c.Id).ToList();
            var paramValue = SetParam(selectedReports, selectedCity);

            ReportTask reportTask = new ReportTask();
            reportTask.ReportKindId = reportTaskVM.ReportKind.Id;
            reportTask.UserId = userId;
            reportTask.Status = "New";
            reportTask.Created = DateTime.Now;            
            reportTask.ParamValue = paramValue;

            repository.SaveReport(reportTask);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CreateSmartChange(ReportTaskVM reportTaskVM)
        {
            var userId = User.Identity.GetUserId();

            ReportTask existingReportTask = repository.ReportTasks.FirstOrDefault(r =>
                   (r.ReportKindId == reportTaskVM.ReportKind.Id
                    && r.UserId == userId
                    && (r.Status == "New") || (r.Status == "InProgress")));

            if (existingReportTask != null)
            {
                return RedirectToAction("Index");
            }

            DeleteReadyReport(reportTaskVM, userId);                        

            ReportTask reportTask = new ReportTask();
            reportTask.ReportKindId = reportTaskVM.ReportKind.Id;
            reportTask.UserId = userId;
            reportTask.Status = "New";
            reportTask.Created = DateTime.Now;
            reportTask.ParamValue = 1032.ToString();

            repository.SaveReport(reportTask);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CreateTurnover(ReportTaskVM reportTaskVM)
        {
            var userId = User.Identity.GetUserId();


            ReportTask existingReportTask = repository.ReportTasks.FirstOrDefault(r =>
                   (r.ReportKindId == reportTaskVM.ReportKind.Id
                    && r.UserId == userId
                    && (r.Status == "New") || (r.Status == "InProgress")));

            if (existingReportTask != null)            
                return RedirectToAction("Index");
            
            DeleteReadyReport(reportTaskVM, userId); //удалить, чтоб работала автозагрузка документов и смена статусов

            var selectedNomenclatures = reportTaskVM.Nomenclatures.Where(n => n.IsChecked).Select(n => n.NomenclatureId).ToList();
            var paramValue = SetTurnoverParam(reportTaskVM.ReportTask.ParamValue, reportTaskVM.Param, selectedNomenclatures);

            ReportTask reportTask = new ReportTask();
            reportTask.ReportKindId = reportTaskVM.ReportKind.Id;
            reportTask.UserId = userId;
            reportTask.Status = "New";
            reportTask.Created = DateTime.Now;
            reportTask.ParamValue = paramValue;

            repository.SaveReport(reportTask);

            return RedirectToAction("Index");
        }

        private string SetParam(List<int> selectedReports, List<int> selectedCity)
        {
            var paramReport = string.Join(",", selectedReports.Select(c => c.ToString()));
            paramReport = paramReport + ";";
            var paramCity = string.Join(",", selectedCity.Select(c => c.ToString()));
            var paramValue = paramReport + paramCity;
            return paramValue;
        }

        private string SetParam(string startDate, string endDate)
        {
            var paramDates = string.Join(",", new string[] { startDate, endDate });                            
            return paramDates;
        }

        private string SetTurnoverParam(string startDate, string endDate, List<string> selectedNomenclatures)
        {
            var paramDates = string.Join(",", new string[] { startDate, endDate });
            paramDates = paramDates + ";";             
            var paramNomenclature = string.Join(",", selectedNomenclatures.Select(n => n.ToString()));
            var paramValue = paramDates + paramNomenclature;
            return paramValue;
        }

        private void DeleteReadyReport(ReportTaskVM reportTaskVM, string userId)
        {
            var readyReportTask = repository.ReportTasks.Where(r =>
               (r.UserId == userId
                && r.ReportKindId == reportTaskVM.ReportKind.Id
                && ((r.Status == "Error") || (r.Status == "Done")))).ToList();

            if (!readyReportTask.Any())
                return;

            foreach (var task in readyReportTask)
            {
                repository.DeleteReport(task.Id);
            }
            
        }

        public ActionResult DownloadReportFileForMobile(int id)                              
        {
            var reportTask = repository.ReportTasks.Where(rt => rt.Id == id).FirstOrDefault();
            if (reportTask == null)            
                return null;
            string fileName = id.ToString() + ".pdf";
            var filePath = reportTask.LinkToReportFile;

            MarkAsDownloaded(id);
            return File(filePath, "application/pdf", fileName);
        }

        public FilePathResult DownloadReportFileForDesktop(int id)
        {
            var reportTask = repository.ReportTasks.Where(rt => rt.Id == id).FirstOrDefault();
            if (reportTask == null)
                return null;
                        
            var filePath = reportTask.LinkToReportFile;

            MarkAsDownloaded(id);
            return new FilePathResult(filePath, "application/pdf");
        }

        public void MarkAsDownloaded(int id)
        {
            var reportTask = repository.ReportTasks.FirstOrDefault(rt => rt.Id == id);
            reportTask.IsDownloaded = true;
            repository.SaveReport(reportTask);
        }

        public ActionResult OpenResultAsText(int id)
        {
            var result = repository.ReportTasks.Where(rt => rt.Id == id).FirstOrDefault();
            return View(result);
        }

        public ActionResult GetModal(int id)
        {
            var reportKind = repository.ReportKinds.FirstOrDefault(rk => rk.Id == id);       
            return PartialView("ModalDialogDatePartialView", reportKind);
        }
    }
}