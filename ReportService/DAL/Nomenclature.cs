﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ReportService.DAL
{
    public class Nomenclature
    {
        public int Id { get; set; }
        public int Category { get; set; }
        public string Name { get; set; }
        public string NomenclatureId { get; set; }
        [NotMapped]
        public bool IsChecked { get; set; }

        public Nomenclature()
        {
            this.IsChecked = false;
        }
    }
}