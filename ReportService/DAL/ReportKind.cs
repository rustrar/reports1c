﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ReportService.DAL
{
    public class ReportKind
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAdmin { get; set; }
        public string ParamType { get; set; }
        public bool RequiredParam { get; set; }
        public byte Order { get; set; }
        public string Size { get; set; }
        [NotMapped]
        public bool IsChecked { get; set; }
    }
}