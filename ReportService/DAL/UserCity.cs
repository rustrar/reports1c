﻿using Microsoft.AspNet.Identity;

namespace ReportService.DAL
{
    public class UserCity
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public IUser User { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
    }
}