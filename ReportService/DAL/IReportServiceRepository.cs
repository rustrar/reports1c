﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportService.DAL
{
    public interface IReportServiceRepository
    {
        IEnumerable<ReportTask> ReportTasks { get; }
        IEnumerable<ReportKind> ReportKinds { get; }
        IEnumerable<City> Cities { get; }
        IEnumerable<Category> Categories{ get; }
        IEnumerable<DayOfWeek> DaysOfWeek{ get; }
        IEnumerable<UserCity> UserCities { get; }
        IEnumerable<Nomenclature> Nomenclatures { get; }
        IEnumerable<NomenclatureUsageStatistic> NomenclatureUsageStatistics { get; }
        void SaveReport(ReportTask reportTask);
        ReportTask DeleteReport(int reportId);
      
    }
}