﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ReportService.DAL
{
    public class EFReportServiceRepository : IReportServiceRepository
    {
        private ReportServiceDB context = new ReportServiceDB();

        public IEnumerable<Category> Categories
        {
            get
            {
                return context.Categories.AsEnumerable<Category>();
            }
        }

        public IEnumerable<City> Cities
        {
            get
            {
                var result = context.Cities.Include(r => r.Category);
                return result;
            }
        }

        public IEnumerable<DayOfWeek> DaysOfWeek
        {
            get
            {
                return context.DaysOfWeek.AsEnumerable<DayOfWeek>();
            }
        }

        public IEnumerable<ReportKind> ReportKinds
        {
            get
            {
                return context.ReportKinds.AsEnumerable<ReportKind>();
            }
        }


        public IEnumerable<ReportTask> ReportTasks
        {
            get
            {
                var result = context.ReportTasks.Include(r => r.ReportKind);
                return result;
            }
        }

        public IEnumerable<UserCity> UserCities
        {
            get
            {
                return context.UserCities.Include(c => c.City);
            }
        }

        public IEnumerable<Nomenclature> Nomenclatures
        {
            get
            {
                return context.Nomenclatures.AsEnumerable<Nomenclature>();
            }
        }

        public IEnumerable<NomenclatureUsageStatistic> NomenclatureUsageStatistics
        {
            get
            {
                return context.NomenclatureUsageStatistics.AsEnumerable<NomenclatureUsageStatistic>();
            }
        }

        public ReportTask DeleteReport(int reportId)
        {
            ReportTask dbEntry = context.ReportTasks.Find(reportId);

            if (dbEntry == null)            
                return dbEntry;
            
            
            context.ReportTasks.Remove(dbEntry);
            context.SaveChanges();
            
            return dbEntry;
        }





        public void SaveReport(ReportTask reportTask)
        {
            if (reportTask.Id == 0)
            {
                context.ReportTasks.Add(reportTask);
            }
            else
            {
                ReportTask dbEntry = context.ReportTasks.Find(reportTask.Id);

                if (dbEntry != null)
                {                    
                    dbEntry.ReportKindId = reportTask.ReportKindId;
                    dbEntry.Status = reportTask.Status;
                    dbEntry.IsDownloaded = reportTask.IsDownloaded;
                }
            }
            context.SaveChanges();
        }
    }
}