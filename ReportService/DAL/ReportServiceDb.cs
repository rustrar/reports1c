﻿using ReportService.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ReportService.DAL
{
    public class ReportServiceDB : DbContext
    {
        public DbSet<ReportTask> ReportTasks { get; set; }
        public DbSet<ReportKind> ReportKinds { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<DayOfWeek> DaysOfWeek { get; set; }
        public DbSet<UserCity> UserCities { get; set; } 
        public DbSet<Nomenclature> Nomenclatures { get; set; }    
        public DbSet<NomenclatureUsageStatistic> NomenclatureUsageStatistics{ get; set; }    
    }
}