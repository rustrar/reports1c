﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ReportService.DAL
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        [NotMapped]
        public bool IsChecked { get; set; }

        public City()
        {
            this.IsChecked = true;
        }
    }    
}