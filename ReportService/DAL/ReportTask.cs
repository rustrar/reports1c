﻿using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel;

namespace ReportService.DAL
{
    public class ReportTask
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public IUser User { get; set; }
        public int ReportKindId { get; set; }
        public ReportKind ReportKind { get; set; }
        public string Status { get; set; }
        [DisplayName("Ссылка на отчет")]
        public string LinkToReportFile { get; set; }
        public string ParamValue { get; set; }        
        public string ReportResultAsText { get; set; }
        public bool IsDownloaded { get; set; }
        public DateTime Created { get; set; }        
    }
}