﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportService.DAL
{
    public class DayOfWeek
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}