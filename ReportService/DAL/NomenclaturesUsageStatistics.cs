﻿using Microsoft.AspNet.Identity;

namespace ReportService.DAL
{
    public class NomenclatureUsageStatistic
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string NomenclatureId { get; set; }        
        public int Count { get; set; }
    }
}