namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveUser : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UserCities", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserCities", "UserId", c => c.String());
        }
    }
}
