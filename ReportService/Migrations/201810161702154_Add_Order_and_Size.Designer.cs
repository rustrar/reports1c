// <auto-generated />
namespace ReportService.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_Order_and_Size : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_Order_and_Size));
        
        string IMigrationMetadata.Id
        {
            get { return "201810161702154_Add_Order_and_Size"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
