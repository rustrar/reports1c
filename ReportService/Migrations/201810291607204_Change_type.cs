namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_type : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Nomenclatures", "NomenclatureId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Nomenclatures", "NomenclatureId", c => c.Int(nullable: false));
        }
    }
}
