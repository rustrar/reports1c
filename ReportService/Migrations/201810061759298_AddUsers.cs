namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUsers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserCities", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserCities", "UserId");
        }
    }
}
