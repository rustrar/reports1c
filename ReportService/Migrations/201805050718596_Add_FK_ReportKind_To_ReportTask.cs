namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_FK_ReportKind_To_ReportTask : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReportKinds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ReportTasks", "ReportKindId", c => c.Int(nullable: false));
            CreateIndex("dbo.ReportTasks", "ReportKindId");
            AddForeignKey("dbo.ReportTasks", "ReportKindId", "dbo.ReportKinds", "Id", cascadeDelete: true);
            DropColumn("dbo.ReportTasks", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ReportTasks", "Name", c => c.String());
            DropForeignKey("dbo.ReportTasks", "ReportKindId", "dbo.ReportKinds");
            DropIndex("dbo.ReportTasks", new[] { "ReportKindId" });
            DropColumn("dbo.ReportTasks", "ReportKindId");
            DropTable("dbo.ReportKinds");
        }
    }
}
