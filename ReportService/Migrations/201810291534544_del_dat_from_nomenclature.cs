namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class del_dat_from_nomenclature : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Nomenclatures", "StartDate");
            DropColumn("dbo.Nomenclatures", "EndDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Nomenclatures", "EndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Nomenclatures", "StartDate", c => c.DateTime(nullable: false));
        }
    }
}
