namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryFKToCity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cities", "CategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Cities", "CategoryId");
            AddForeignKey("dbo.Cities", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cities", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Cities", new[] { "CategoryId" });
            DropColumn("dbo.Cities", "CategoryId");
        }
    }
}
