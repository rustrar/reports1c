namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rename_Report_To_ReportTask : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Reports", newName: "ReportTasks");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.ReportTasks", newName: "Reports");
        }
    }
}
