namespace ReportService.Migrations
{    
    using System.Data.Entity.Migrations;
    
    public partial class Add_Statistics_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.NomenclatureUsageStatistics",
                    c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        NomenclatureId = c.String(),
                        Count = c.Int(nullable: false),
                        Nomenclature_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);

            //CreateIndex("dbo.Nomenclatures", "NomenclatureId");
            //AddForeignKey("dbo.Nomenclatures", "NomenclatureId", "dbo.NomenclatureUsageStatistics", "NomenclatureId", cascadeDelete: false);
            //.ForeignKey("dbo.Nomenclatures", t => t.Nomenclature_Id)
            //.Index(t => t.Nomenclature_Id);
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.NomenclatureUsageStatistics", "NomenclatureId", "dbo.Nomenclatures");
            //DropIndex("dbo.NomenclatureUsageStatistics", new[] { "NomenclatureId" });
            DropTable("dbo.NomenclatureUsageStatistics");
        }
    }
}
