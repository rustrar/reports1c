namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ReportResultAsText_Columnt_To_ReportTask : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportTasks", "ReportReultAsText", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReportTasks", "ReportReultAsText");
        }
    }
}
