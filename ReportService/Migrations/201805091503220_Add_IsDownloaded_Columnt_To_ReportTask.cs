namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_IsDownloaded_Columnt_To_ReportTask : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportTasks", "IsDownloaded", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReportTasks", "IsDownloaded");
        }
    }
}
