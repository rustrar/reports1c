namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_LinkToReportFile_Column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportTasks", "LinkToReportFile", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReportTasks", "LinkToReportFile");
        }
    }
}
