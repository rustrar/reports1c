namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_UserCity_and_CityColumn_to_ReportTask : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserCities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
            AddColumn("dbo.ReportTasks", "City", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserCities", "CityId", "dbo.Cities");
            DropIndex("dbo.UserCities", new[] { "CityId" });
            DropColumn("dbo.ReportTasks", "City");
            DropTable("dbo.UserCities");
        }
    }
}
