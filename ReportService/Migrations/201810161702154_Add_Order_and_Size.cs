namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Order_and_Size : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportKinds", "Order", c => c.Byte(nullable: false));
            AddColumn("dbo.ReportKinds", "Size", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReportKinds", "Size");
            DropColumn("dbo.ReportKinds", "Order");
        }
    }
}
