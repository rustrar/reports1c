namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Params_To_ReportKind : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportKinds", "ParamType", c => c.String());
            AddColumn("dbo.ReportKinds", "RequiredParam", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReportKinds", "RequiredParam");
            DropColumn("dbo.ReportKinds", "ParamType");
        }
    }
}
