namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ParamValue_Column_ToReportTask : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportTasks", "ParamValue", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReportTasks", "ParamValue");
        }
    }
}
