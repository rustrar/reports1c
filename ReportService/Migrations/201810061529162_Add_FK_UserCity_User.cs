namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_FK_UserCity_User : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserCities", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserCities", "UserId", c => c.Int(nullable: false));
        }
    }
}
