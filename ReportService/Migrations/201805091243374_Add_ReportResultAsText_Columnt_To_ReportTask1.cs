namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ReportResultAsText_Columnt_To_ReportTask1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportTasks", "ReportResultAsText", c => c.String());
            DropColumn("dbo.ReportTasks", "ReportReultAsText");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ReportTasks", "ReportReultAsText", c => c.String());
            DropColumn("dbo.ReportTasks", "ReportResultAsText");
        }
    }
}
