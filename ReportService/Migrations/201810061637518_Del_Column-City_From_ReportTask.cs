namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Del_ColumnCity_From_ReportTask : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ReportTasks", "City");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ReportTasks", "City", c => c.Int(nullable: false));
        }
    }
}
