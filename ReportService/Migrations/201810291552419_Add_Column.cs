namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nomenclatures", "NomenclatureId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nomenclatures", "NomenclatureId");
        }
    }
}
