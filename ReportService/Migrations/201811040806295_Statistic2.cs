namespace ReportService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Statistic2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.NomenclatureUsageStatistics", "Nomenclature_Id", "dbo.Nomenclatures");
            DropIndex("dbo.NomenclatureUsageStatistics", new[] { "Nomenclature_Id" });
            DropColumn("dbo.NomenclatureUsageStatistics", "Nomenclature_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.NomenclatureUsageStatistics", "Nomenclature_Id", c => c.Int());
            CreateIndex("dbo.NomenclatureUsageStatistics", "Nomenclature_Id");
            AddForeignKey("dbo.NomenclatureUsageStatistics", "Nomenclature_Id", "dbo.Nomenclatures", "Id");
        }
    }
}
