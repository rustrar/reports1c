﻿$(document).ready(function () {
    PrepareCheckbox();   
});



function DownloadReport(id) {
    sleep(3000).then(() => {
        if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent))             
        {            
            window.location.assign('/Home/DownloadReportFileForMobile/' + id);
        }
        else {            
            document.getElementById(id).click();
        }                
    });
}


function searchNomenclatures() {
    function  Contains(text_one, text_two) {
        if (text_one.indexOf(text_two) != -1)
            return true;
    }    
    if ($("#Search").val().length <= 1) {
        $("#n_category_1").prop("checked", false);
        $("#n_category_2").prop("checked", false);
    }    

    var searchText = $("#Search").val().toLowerCase();
    $(".nomenclature-item").each(function() {
        if (!Contains($(this).text().toLowerCase(), searchText)) {
            $(this).hide();                
        }
        else
            $(this).show();               
    });   

    $(".nomenclature-count_cat1").text("ВРН (" + $(".n_cat_1:visible").length +")");
    $(".nomenclature-count_cat2").text("СПБ (" + $(".n_cat_2:visible").length + ")");
}

function PrepareCheckbox() {
    document.getElementById("1022").checked = true;
}

function toggleCheckbox(element) {
    var chkBoxId = element.id;
    if (chkBoxId == 'category_1') {
        if (element.checked) {
            $(".city_cat_1").prop("checked", true);
        }
        else {            
            $('.city_cat_1').prop('checked', false);            
        }
    } else if (chkBoxId == 'category_2') {
        if (element.checked) {
            $(".city_cat_2").prop("checked", true);
        }
        else {
            $(".city_cat_2").prop("checked", false);
        }
    } else if (chkBoxId == 'category_3') {
        if (element.checked) {
            $(".city_cat_3").prop("checked", true);
        }
        else {
            $(".city_cat_3").prop("checked", false);
        }
    }
    SetButton();
}

function n_toggleCheckbox(element) {
    var chkBoxId = element.id;
    if (chkBoxId == 'n_category_1') {
        if (element.checked) {
            $(".n_cat_1:visible").prop("checked", true);
        }
        else {            
            $('.n_cat_1:visible').prop('checked', false);            
        }
    } else if (chkBoxId == 'n_category_2') {
        if (element.checked) {
            $(".n_cat_2:visible").prop("checked", true);
        }
        else {
            $(".n_cat_2:visible").prop("checked", false);
        }
    }
    SetButtons();
}

function toggleChangeReport(element) {
    var chkId = element.id;
    if ( (chkId == '1019' && element.checked ) || (chkId == '1021' && element.checked) ) {
        $('#1022').prop("checked", false);        
    }
    if (chkId == '1022' && element.checked) {
        $('#1019').prop("checked", false);
        $('#1021').prop("checked", false);
    }
    SetButton();
}


function SetButton() {
    if (($('.myCheckBox:checked').length >= 1) && ($('.myReportCheckBox:checked').length >= 1)) {
        $('.change-button').prop("disabled", false);
    }
    else {
        $('.change-button').prop("disabled", true);
    }
}

function SetButtons() {
    if (($('.myCheckBox:checked').length >= 1) && ($('.n_cat:checked').length >= 1)) {
        $('.change-button').prop("disabled", false);
    }
    else {
        $('.change-button').prop("disabled", true);
    }
}

$(window).on('hidden.bs.modal', function () {
    $.connection.hub.start();
    var reportTaskDiv = $('#reporttask');

    $.ajax({
        type: 'GET',
        url: '/Home/RefreshAndDownload',
        contentType: 'application/html ; charset:utf-8',
        dataType: 'html',
        success: function (result) {
            reportTaskDiv.empty().append(result);
            $(".selectpicker").selectpicker();
            PrepareCheckbox();
        },
        error: function () {       
        }
    });
});

$(window).on('shown.bs.modal', function () {
    $.connection.hub.stop();
});