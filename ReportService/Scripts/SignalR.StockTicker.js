﻿// Crockford's supplant method
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

$(function () {
    var ticker = $.connection.reportTaskTicker; // the generated client-side hub proxy
 

    ticker.client.updateReportTaskLink = function () {
        refreshAndDownloadFiles();
    };

    function refreshAndDownloadFiles(){                
        var reportTaskDiv = $('#reporttask');        

        $.ajax({
            type: 'GET',            
            url: '/Home/RefreshAndDownload',
            contentType: 'application/html ; charset:utf-8',            
            dataType: 'html',
            success: function (result) {                
                reportTaskDiv.empty().append(result);
                $(".selectpicker").selectpicker();
                PrepareCheckbox();
            },
            error: function () {
                //alert("error");
            }
        });                
    }

    $.connection.hub.start();
});

