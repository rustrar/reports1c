﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using ReportService.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using TableDependency;
using TableDependency.Enums;
using TableDependency.EventArgs;
using TableDependency.SqlClient;

namespace ReportService.Hubs
{
    public class ReportTaskTicker
    {
        private EFReportServiceRepository repository = new EFReportServiceRepository();

        // Singleton instance
        private readonly static Lazy<ReportTaskTicker> _instance = new Lazy<ReportTaskTicker>(
            () => new ReportTaskTicker(GlobalHost.ConnectionManager.GetHubContext<ReportTaskTickerHub>().Clients));

        private static SqlTableDependency<ReportTask> _tableDependency;

        private ReportTaskTicker(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;

            var mapper = new ModelToTableMapper<ReportTask>();
            mapper.AddMapping(s => s.Status, "Status");

            _tableDependency = new SqlTableDependency<ReportTask>(
                ConfigurationManager.ConnectionStrings["ReportServiceDB"].ConnectionString,
                "ReportTasks",
                mapper);

            _tableDependency.OnChanged += SqlTableDependency_Changed;
            _tableDependency.OnError += SqlTableDependency_OnError;
            _tableDependency.Start();
        }

        public static ReportTaskTicker Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }

        public IEnumerable<ReportTask> GetAllReportTasks()
        {
            var ReportTaskModel = new List<ReportTask>();

            //var connectionString = ConfigurationManager.ConnectionStrings
            //        ["connectionString"].ConnectionString;
            //using (var sqlConnection = new SqlConnection(connectionString))
            //{
            //    sqlConnection.Open();
            //    using (var sqlCommand = sqlConnection.CreateCommand())
            //    {
            //        sqlCommand.CommandText = "SELECT * FROM [ReportTasks]";

            //        using (var sqlDataReader = sqlCommand.ExecuteReader())
            //        {
            //            while (sqlDataReader.Read())
            //            {
            //                var code = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Code"));
            //                var name = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Name"));
            //                var price = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Price"));

            //                ReportTaskModel.Add(new ReportTask { Symbol = code, Name = name, Price = price });
            //            }
            //        }
            //    }
            //}
            return repository.ReportTasks.AsEnumerable();
            //return ReportTaskModel;
        }

        void SqlTableDependency_OnError(object sender, ErrorEventArgs e)
        {
            //throw e.Error;
        }

        /// <summary>
        /// Broadcast New ReportTask Price
        /// </summary>
        void SqlTableDependency_Changed(object sender, RecordChangedEventArgs<ReportTask> e)
        {
            if (e.ChangeType == ChangeType.Update)
            {
                BroadcastReportTaskLinkToReportFile(e.Entity);
            }
        }

        private void BroadcastReportTaskLinkToReportFile(ReportTask ReportTask)
        {
            Clients.All.updateReportTaskLink(ReportTask);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _tableDependency.Stop();
                }

                disposedValue = true;
            }
        }

        ~ReportTaskTicker()
        {
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}