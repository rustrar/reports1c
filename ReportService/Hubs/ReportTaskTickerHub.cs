﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using ReportService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportService.Hubs
{
    [HubName("reportTaskTicker")]
    public class ReportTaskTickerHub : Hub
    {
        private readonly ReportTaskTicker _reportTaskTicker;

        public ReportTaskTickerHub() :
        this(ReportTaskTicker.Instance)
    {

        }

        public ReportTaskTickerHub(ReportTaskTicker reportTaskTicker)
        {
            _reportTaskTicker = reportTaskTicker;
        }

        public IEnumerable<ReportTask> GetAllReportTasks()
        {
            return _reportTaskTicker.GetAllReportTasks();
        }
    }
}